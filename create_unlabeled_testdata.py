#!/usr/bin/python3
import sys
import os
import collections
from importlib.machinery import SourceFileLoader

def special_char(str):
	if ":" in str:
		str_mod=str[:-1]+"colon";
		return str_mod;
	if "/" in str:
		str_mod=str[:-1]+"slash";
		return str_mod;
	return str;

def main():
	a={}
	
	try:
		path=sys.argv[1]
		files=os.listdir(path)
	except (OSError) as e:
		print("Please give the path to train directory")
		sys.exit()
	
	for file in sorted(files):
		flag=0;
		if not file.startswith('.'):
			foo = SourceFileLoader("module.name", "hw3_corpus_tool.py").load_module()
			file=path+"/"+file
			a=foo.get_utterances_from_filename(file)
			last_speaker=""
			prev_token=""
			for x in a:
				str=""
				
				flag_token=""
				speaker=x[1]
				list_postag=[]
				list_postag=x[2]
				pos_string=""
				token_string=""
				if list_postag is not None:
					for temp in list_postag:
						spl_pos=special_char(temp.pos)
						pos_string+="POS_"+spl_pos+"\t"
					
					list_token=[]
					list_token=x[2]
					for temp in list_token:
						spl_token=special_char(temp.token)
						token_string+="TOKEN_"+spl_token+"\t"
					
					if token_string.endswith(" "): token_string = token_string[:-1]
					if pos_string.endswith(" "): pos_string = pos_string[:-1]
				else:							#ignoring
					continue;
					
				if flag==0:
					y=""
					str="first_utterance"
				else:
					if speaker==last_speaker:
						y=""
					else:
						y="1"
				flag+=1;
				#flag_token=question_token(x[3]);
			#print((list_postag[0]).pos);
				if y=="":
					print("UNK\t"+token_string+pos_string+str)
				else:
					print("UNK\t"+y+"\t"+token_string+pos_string+str)
				last_speaker=speaker
				prev_token=flag_token;
			print("")
			

if __name__ == '__main__':
    main()
