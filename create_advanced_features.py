#!/usr/bin/python3
import sys
import os
import collections
from importlib.machinery import SourceFileLoader
def question_token(str):
	if "?" in str or "what" in str or "when" in str or "how" in str or "why" in str or "where" in str:
		return "true\t";
	else:
		return "";
def special_char(str):
	if ":" in str:
		str_mod=str[:-1]+"colon";
		return str_mod;
	if "/" in str:
		str_mod=str[:-1]+"slash";
		return str_mod;
	return str;
def bigram_tok(list_token):
	bigram_token="\t"
	i=0;
	for i in range(len(list_token)):
		spl_token=special_char(list_token[i].token)
		if i==(len(list_token)-1):
			bigram_token+=spl_token+",\t"
			break;
		else:
			j=i+1;
			spl_nexttoken=special_char(list_token[j].token)
			bigram_token+=spl_token+","+spl_nexttoken+"\t"
	return bigram_token;
def bigram_pos(list_pos):
	for i in range(len(list_postag)):
		spl_pos=special_char(list_postag[i].pos)
		if i==(len(list_postag)-1):
			bigram_pos+=spl_pos+",\t"
			break;
		else:
			j=i+1;
			spl_nextpos=special_char(list_postag[j].pos)
			bigram_pos+=spl_pos+","+spl_nextpos+"\t"
	return bigram_pos;
	
def main():
	a={}
	''''
	try:
		path=sys.argv[1]
		files=os.listdir(path)
	except (OSError) as e:
		print("Please give the path to train directory")
		sys.exit()
	
	for file in sorted(files):
		flag=0;
		if not file.startswith('.'):
	'''		
	foo = SourceFileLoader("module.name", "hw3_corpus_tool.py").load_module()
	try:
		file=sys.argv[1]
	except (OSError) as e:
		print("Please give the path to train directory")
		sys.exit()
	utterance_tuples=foo.get_utterances_from_filename(file)
	last_speaker=""
	prev_token=""
	flag=0;
	for x in utterance_tuples:
		str=""
				
		flag_token=""
		speaker=x[1]
		list_postag=[]
		list_postag=x[2]
		pos_string=""
		token_string=""
		if list_postag is not None:
			for temp in list_postag:
				spl_pos=special_char(temp.pos)
				pos_string+="POS_"+spl_pos+"\t"
				
			list_token=[]
			list_token=x[2]
			for temp in list_token:
				spl_token=special_char(temp.token)
				token_string+="TOKEN_"+spl_token+"\t"
			bigram_token=""
			bigram_token=bigram_tok(list_token)
					
		else:							#ignoring
			continue;
					
		if flag==0:
			y=""
			str="first_utterance"
		else:
			if speaker==last_speaker:
				y=""
			else:
				y="1"
		flag+=1;
				#flag_token=question_token(x[3]);
			#print((list_postag[0]).pos);
				
		if y=="":
			print(x[0]+"\t"+token_string+pos_string+bigram_token+str)
		else:
			print(x[0]+"\t"+y+"\t"+token_string+pos_string+bigram_token+str)
			last_speaker=speaker
			prev_token=flag_token;
				
	print("")
			

if __name__ == '__main__':
    main()
