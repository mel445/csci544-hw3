#!/usr/bin/python3
import sys
import os
import collections
from importlib.machinery import SourceFileLoader

def special_char(str):
	if ":" in str:
		str_mod=str[:-1]+"colon";
		return str_mod;
	if "/" in str:
		str_mod=str[:-1]+"slash";
		return str_mod;
	return str;

def main():
	a={}
	
	try:
		file=sys.argv[1]
	except (OSError) as e:
		print("Please give the path to train directory")
		sys.exit()
	
	flag=0;
	foo = SourceFileLoader("module.name", "hw3_corpus_tool.py").load_module()
			
	a=foo.get_utterances_from_filename(file)
	last_speaker=""
	prev_token=""
	for x in a:
		str=""
				
		flag_token=""
		speaker=x[1]
		list_postag=[]
		list_postag=x[2]
		pos_string=""
		token_string=""
		if list_postag is not None:
			for temp in list_postag:
				spl_pos=special_char(temp.pos)
				pos_string+="POS_"+spl_pos+"\t"
					
			list_token=[]
			list_token=x[2]
			for temp in list_token:
				spl_token=special_char(temp.token)
				token_string+="TOKEN_"+spl_token+"\t"
		else:							#ignoring
			continue;
					
		if flag==0:
			y=""
			str="first_utterance"
		else:
			if speaker==last_speaker:
				y=""
			else:
				y="1"
		flag+=1;
				#flag_token=question_token(x[3]);
			#print((list_postag[0]).pos);
		if y=="":
			print(x[0]+"\t"+token_string+pos_string+str)
		else:
			print(x[0]+"\t"+y+"\t"+token_string+pos_string+str)
		last_speaker=speaker
		prev_token=flag_token;
	print("")
			

if __name__ == '__main__':
    main()
